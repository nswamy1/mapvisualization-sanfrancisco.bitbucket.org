'use strict';

myApp.factory("apiService",function($http,$q,XfrmDataService){
    
    return {

        getMapdata : function(url,typeOfData){
            //console.log(url)
            var deferred = $q.defer();
            var param = $.param({})
            var Items = $http({method : 'GET', url:url }).
                            success(function(data, status, headers, config) {
								//console.log(data);

                                if(typeOfData == 'routedata')
                                    deferred.resolve(XfrmDataService.routedata(data));
                                else if(typeOfData == 'locationData')
                                    deferred.resolve(XfrmDataService.locationData(data));
                                else
                                    deferred.resolve(data);

                            }).error(function(data, status, headers, config) {
                                deffered.reject(status);
                            })

            return deferred.promise;
        }

        
    };
});