'use strict';

myApp.factory("XfrmDataService",function($http,$q){
    
    return {

        routedata : function(data){
            data = $.parseXML(data);

                var obj = {};

                    obj.stops = [].map.call(data.querySelectorAll("stop"), function(stop) {     
                            return {
                              stopTag: stop.getAttribute("tag"),
                              stopId:stop.getAttribute("stopId"),
                              title:stop.getAttribute("title"),
                              lat: stop.getAttribute("lat"),
                              lon: stop.getAttribute("lon")
                            };
                          });

                    obj.paths = [].map.call(data.querySelectorAll("path"), function(path) { 

                                var points = [].map.call(path.querySelectorAll("point"), function(point) { 
                                                    return {
                                                              
                                                              lat: point.getAttribute("lat"),
                                                              lon: point.getAttribute("lon")
                                                            };
                                                })
                            return {
                                      path: points
                                    };
                          });
            return obj;
            
        },
        locationData : function(data){
            data = $.parseXML(data);

            data = [].map.call(data.querySelectorAll("vehicle"), function(vehicle) {        
                    return {
                      vehicleId: vehicle.getAttribute("id"),
                      lat: vehicle.getAttribute("lat"),
                      lon: vehicle.getAttribute("lon"),
                      heading:vehicle.getAttribute("heading")
                    };
                  });

            return data;
            
        }


        
    };
});