"use strict";
angular.module('config', [])
.constant('ENV', {
        'BASE_map_URL' : 'data/sfmaps/streets.json',
        'BASE_map_neighbour_URL' : 'data/sfmaps/neighborhoods.json',
        'location_URL'  : "http://webservices.nextbus.com/service/publicXMLFeed?command=vehicleLocations&a=sf-muni&r=N&t=",
        'route_URL'  : "http://webservices.nextbus.com/service/publicXMLFeed?command=routeConfig&a=sf-muni&r=N"
        //"http://webservices.nextbus.com/service/publicXMLFeed?command=routeConfig&a=collegetown&r=blue"
        //"http://webservices.nextbus.com/service/publicXMLFeed?command=routeConfig&a=sf-muni&r=N"
        //
        //
});