'use strict';

var geoMap = angular.module('SFMap-directive',[]);


geoMap.directive('sfmap', function ($http,apiService) {

                return {
                                
                                scope:{                                                                
                                        data: '=',
                                        configdata :'=',
                                        id:'@',
                                        api: '=',
                                        locationdata:'=locationdata',
                                        routedata:"=routedata"
                                },
                                restrict: 'EA',
                                replace: true,
                                link:function(scope, element, attrs){

                                                scope.api = scope.api || {};

                                                if(scope.id == null || scope.id == undefined)
                                                        return;

                                                if(!scope.chartObject)
                                                        scope.chartObject = d3.SFMap()
                                                                                .containerId(scope.id);

                                                scope.api.zoom = scope.chartObject.zoom;

                                                if(!scope.Chart_main)
                                                        scope.Chart_main = d3.select('#'+scope.id)
                                                                                .append('svg')
                                                                                .call(scope.chartObject);

                                                scope.$watch('configData',function(){
                                                        apiService.getMapdata(scope.configdata.baseMapUrl).then(function(data,error){
                                                                scope.chartObject.renderBaseMap(data);
                                                        })
                                                },true);

                                                scope.$watch('locationdata',function(){
                                                                if(scope.locationdata == undefined || !scope.locationdata.length)
                                                                        return;
                                                                scope.chartObject.locationData(scope.locationdata);
                                                },true);

                                                scope.$watch('routedata',function(){
                                                                
                                                                if(scope.routedata == undefined)
                                                                        return;
                                                                if(scope.routedata.stops.length !== 0)
                                                                        scope.chartObject.stopsData(scope.routedata.stops)
                                                                if(scope.routedata.paths.length !== 0)
                                                                        scope.chartObject.routes(scope.routedata.paths) 

                                                },true);

                                }

                }



})
