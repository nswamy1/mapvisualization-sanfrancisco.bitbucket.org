//'use strict';

var myApp = angular.module('SFApp',[
					
				    'ngResource',
				    'ngRoute',
					'ui.bootstrap',
				    'config',
				    'SFMap-directive'
				    
	]).config(function ($routeProvider,ENV) {

      $routeProvider
	      .when('/', {
	          templateUrl: 'scripts/views/home.html',
	          controller: 'homeCtrl'
	      })
	      .otherwise({
	          redirectTo: '/'
	      });
      
  });