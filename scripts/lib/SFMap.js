d3 = d3 || {};

(function(_){
	
	d3.SFMap = function(){

		var state = {},containerId,g_,projection,path,baseUrl;
		var width,height,container,locationData;
		var defaultzoom = 400000
		var bGroundDrag = d3.behavior.drag();
		var vehicleLocationMap = {};
		var capturedLatLon;

		var chart = function(selection){

			selection.each(function(d){

				width = parseInt(d3.select('#'+containerId).style('width'));
				height = parseInt(d3.select('#'+containerId).style('height'));

				container = d3.select(this);

				container.attr('width',width).attr('height',height);


				if(!g_){


					g_ = container.append('g').attr('class','parent_g');

						g_.append('g').attr('class','baseMap')
						g_.append('g').attr('class','bGround').append('rect');
						g_.append('g').attr('class','routes');
						g_.append('g').attr('class','stops');
						g_.append('g').attr('class','location');

					container.append('g').attr('class','fdrag').append('rect');

				}


				container.select('.bGround').select('rect')
						.attr('x',0)
						.attr('y',0)
						.attr('width',width)
						.attr('height',height)
						.style('fill','transparent')
						.on('click',function(){
								console.log(d3.mouse(this))
								console.log(projection.invert(d3.mouse(this)))
								capturedLatLon = projection.invert(d3.mouse(this));
							})
				
				g_.call(bGroundDrag)

				projection = d3.geo.albers()
							    .scale(defaultzoom)
							    .translate([width/2, height/2 ]);
				path = d3.geo.path()
    		  					.projection(projection)


			})

		}

		bGroundDrag.on('drag',function(e){
			var newTransform = d3.transform(g_.attr('transform')).translate;

				newTransform[0] = newTransform[0]+d3.event.dx;
				newTransform[1] = newTransform[1]+d3.event.dy;

				d3.select(this).select('.bGround')
								.select('rect')
								.attr('x',newTransform[0]*-1)
								.attr('y',newTransform[1]*-1);

				g_.attr('transform','translate('+(newTransform[0])+','+(newTransform[1])+')');

		})

		chart.zoom = function(_){
			zoom = true;
			defaultzoom = defaultzoom + _;
			projection.scale(defaultzoom);
			path.projection(projection);
			chart.renderBaseMap(state.mapData);
		};

		chart.baseUrl = function(_){
			baseUrl = _;
			state.baseUrl = _;
			return chart;
		};

		chart.containerId = function(_){
			containerId = _;
			state.containerId = _;
			return chart;
		};

		chart.renderBaseMap = function(_){
				state.mapData = _;

			var lon = d3.min(state.mapData.features,function(d){
							return d3.min(d.geometry.coordinates,function(e){
								return e[0];
							})
						});
			var lonMax = d3.max(state.mapData.features,function(d){
							return d3.max(d.geometry.coordinates,function(e){
								return e[0];
							})
						});
			var lat = d3.min(state.mapData.features,function(d){
							return d3.min(d.geometry.coordinates,function(e){
								return e[1];
							})
						});
			var latMax = d3.max(state.mapData.features,function(d){
							return d3.max(d.geometry.coordinates,function(e){
								return e[1];
							})
						});
			var co = capturedLatLon?projection(capturedLatLon):projection([lon,lat]);
			var coMax = projection([lonMax,latMax]);

				londiff = coMax[0]-co[0];
				latdiff = coMax[1]-co[1];

				

			if(capturedLatLon){

				container.select('.bGround').select('rect')
											.attr('x',((co[0])-width/2)).attr('y',((co[1])-height/2));
				
				var baseMap = g_.attr('transform','translate('+((co[0]*-1)+width/2)+','+((co[1]*-1)+height/2)+')')
							.select('.baseMap')
						  	.selectAll("path")
						    .data(state.mapData.features);


			}else{

				container.select('.bGround').select('rect')
											.attr('x',(((co[0]+londiff/4))-width/6)).attr('y',(((co[1]))-height/1.25));

				var baseMap = g_.attr('transform','translate('+(((co[0]+londiff/4)*-1)+width/6)+','+(((co[1])*-1)+height/1.25)+')')
							.select('.baseMap')
						  	.selectAll("path")
						    .data(state.mapData.features);
			}

				baseMap
					.enter()
					.append("path");

				baseMap.exit().remove();

				baseMap
				    .attr("d", path)
				    .style('stroke','grey')
					.style('stroke-width','0.1px')
					.style('fill','#BBA543');


				if(state.routes)
					chart.routes(state.routes);
				if(state.stopsData)
					chart.stopsData(state.stopsData);
				if(state.locationData)					
					chart.locationData(state.locationData,true);
		};

		chart.locationData = function(data,zoom){

				state.locationData = data;
					
				vehicleLocationMap = d3.map(JSON.parse(JSON.stringify(data)),function(d){ return d.vehicleId })._;
					
				d3.values(vehicleLocationMap).forEach(function(d){
					d.width = 50;
					d.height = 25;
				});
				
			var vechicles = g_
								.select('.location')
							  	.selectAll(".img")
							    .data(data,function(d){return d.vehicleId});

				vechicles
					.enter()
					.append("g")
					.attr('class','img')
					.attr('transform',function(d){
							var mappedLatlon = projection([d.lon,d.lat]);

						return 'translate('+(mappedLatlon[0]-(30/2))+','+(mappedLatlon[1]-(15/2))+')';
					});

				vechicles.exit().transition().duration(1000).remove();

				vechicles
					.transition()
					.duration(zoom?0:3000)
					.attr('transform',function(d){
						var mappedLatlon = projection([d.lon,d.lat]);

						return 'translate('+(mappedLatlon[0]-(30/2))+','+(mappedLatlon[1]-(15/2))+')';
					})
					.each(function(d){
						var img =	d3.select(this)
										.selectAll('image')
										.data([d]);
							img.enter()
								.append('svg:image')
								.attr('x',0)
								.attr('y',0)
			                    .attr('height',15)
			                    .attr('width',30)
								.on('mouseover',function(d){
			                    	console.log(d);
			                    	console.log(this);
			                    })
			                    .on('click',function(d){
									 //console.log(d3.mouse(this))
									 var translate = d3.transform(d3.select(this.parentNode).attr('transform')).translate
									 console.log(projection.invert(d3.mouse(this)))

									capturedLatLon = projection.invert(translate);
								})

							img.exit().remove();

							img
								.attr('xlink:href',function(d){
                                    return 'newBusIcon_.png';
                                    //'buswithdirection.png'
			                    })
								.attr('transform',function(d){
									var angle = (d.heading < 0)?0:(parseInt(d.heading)+120)%360;

									var ind = (angle < -45)||(angle > 150 && angle < 250)?true:false;
									var yOffset = ind?0:15/2;
									return 'rotate('+(angle)+' '+(30/2)+' '+(yOffset)+') scale(1,'+(ind?-1:1)+')';
								});

					});
				
		}

		chart.stopsData = function(data){

				state.stopsData =data;
			var stops = g_
								.select('.stops')
							  	.selectAll("circle")
							    .data(data);

				stops
					.enter()
					.append("circle")
				    .attr("r", 3)
				    .style('fill','white')
				    .style('stroke','orange')
					.style('stroke-width','1px');

				stops.exit().attr('r',0).remove();

				stops
					.attr('cx',function(d){
				    	return projection([d.lon,d.lat])[0];
				    })
				    .attr('cy',function(d){
				    	return projection([d.lon,d.lat])[1];
				    });
				
		}
		chart.routes = function(data){
				
				state.routes =data;
			var routes = g_
								.select('.routes')
							  	.selectAll("path")
							    .data(data);

				routes
					.enter()
					.append("path")
				    .style('stroke','orange')
					.style('stroke-width','1px')
					.style('fill','none');

				routes.exit().attr('r',0).remove();

				routes
					.attr("d", function(d){
				    	var str = "M"
				    		d.path.forEach(function(_,i){
				    			var _i = projection([_.lon,_.lat])
				    			if(i==0)
				    				str = str+_i[0]+','+_i[1];
				    			else
				    				str = str+' L'+_i[0]+','+_i[1];
				    		});
				    		str = str;

				    	return str;
				    });
				
		}
		chart.handleVehicleOverlap = function(vehicle){

				var mappedLatlon = projection([vehicle.lon,vehicle.lat]);
				var currentVehicle = vehicleLocationMap[vehicle.vehicleId];

							d3.values(vehicleLocationMap).forEach(function(e){
								if(e.vehicleId !== currentVehicle.vehicleId){
									if((Math.abs(currentVehicle.x+currentVehicle.xOffset - e.x+currentVehicle.xOffset) <50)
										&& (Math.abs(currentVehicle.y+currentVehicle.yOffset - e.y+currentVehicle.yOffset) <25)
										){
										console.log('conflict');


									}

								}
								
							});
		}

		chart.rescaleLonLat = function(lon,lat){

		}

		return chart;

	}
		
})(window)