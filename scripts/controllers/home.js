'use strict';
myApp.controller('homeCtrl',function ($scope,$rootScope,apiService,ENV){



	$scope.chartId = "chart";
	$scope.control = {};
	$scope.chartConfig = {baseMapUrl:ENV.BASE_map_URL};
	$scope.data = {};

	var d = new Date();

	apiService.getMapdata(ENV.route_URL,'routedata').then(function(data,error){
			//triggers updateRoute method inside Directive
			$scope.routeData = data;
			//Pull Bus Locations
			$scope.updateLocation();
	});

	//Keep Pulling Bus Location once in every 15 seconds
	setInterval(function(){
		$scope.updateLocation();
	},15000)
	

	$scope.updateLocation = function(){
		var dd = new Date();
		apiService.getMapdata(ENV.location_URL+(Date.parse(dd)-Date.parse(d)),'locationData').then(function(data,error){
			$scope.locationData = data;
		});

	}
	$scope.zoom = function(_){
		if(typeof $scope.control.zoom == 'function')
			$scope.control.zoom(_)
	}
	

});